# Virtuous #

# Purpose #

Virtuous is a web application which requires its users to log in through Facebook. It then extracts their Facebook posts and likes. This extracted data is made to go through sentiment analysis via NLP (Natural Language Processing). All the strings are provided a numeric value according to their sentiment analysis. This value is used to cluster the users through which users can find other people (who may or may not be friends on Facebook) who are similar in terms of the way they "talk" and "like" on Facebook.

# Project Scope #

The system focuses on only one class of user, interacting with the system directly. Through the system, the various users will be able to find other people (who may or may not be friends on facebook) which are similar in terms of the way they "talk" and "like" on facebook.

# Operating Environment #

1. Latest web browser
2. Working internet connection