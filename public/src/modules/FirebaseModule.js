var TAG='FirebaseModule';

var FirebaseModule={

	url:'https://my-words.firebaseio.com',
	ref:null,
	connectedRef:null,
	users:null,
	keys:{
		user_login:'users',
		user_data:'mock_users'
	},
	connection_available:false,

	init:function(){
		UtilityModule.log(TAG,'init');
		var self=this;
		self.ref=new Firebase(self.url);
		self.ref.child(self.keys.user_data).on('value',function(snapshot){
			UtilityModule.log(TAG,snapshot);
			self.users=snapshot.val();
		},function(errorObject){
			UtilityModule.log(TAG,errorObject);
		});
		self.connectedRef=new Firebase(self.url+'/.info/connected');
		self.connectedRef.on('value',function(snap) {
			if(snap.val()===true) {
				UtilityModule.log(TAG,'connection established');
				self.connection_available=true;
		  	}else{
				UtilityModule.log(TAG,'connection broken');
				self.connection_available=false;
		  	}
		});
	},

	fb_login:function(next){
		UtilityModule.log(TAG,'fb_login');
		var self=this;
		self.ref.authWithOAuthPopup('facebook', function(error, authData) {
			if (error) {
				UtilityModule.log(TAG,'fb login failed');
				UtilityModule.log(TAG,error);
				next(error);
		  	} else {
		  		UtilityModule.log(TAG,'fb login successful');
		    	UtilityModule.log(TAG,authData.facebook.accessToken);
		    	FbModule.access_token=authData.facebook.accessToken;
		    	FbModule.get_me(function(response){
		    		FbModule.get_posts(function(response){
						FbModule.get_likes(function(response){
							self.update_user(FbModule.id,FbModule.name,FbModule.posts,FbModule.likes,next);
						});
					});
		    	});
		  	}
		}, {
			scope:FbModule.permissions
		});
	},

	update_user:function(id,name,posts,likes,next){
		UtilityModule.log(TAG,'update_user');
		var self=this;
		var user=self.ref.child(self.keys.user_login);
		var json={};
		json[id]={
			'name':name,
			'posts':posts,
			'likes':likes
		}
		user.update(json,next);
	},

	download_users:function(next){
		UtilityModule.log(TAG,'download_users');
		var self=this;
		UtilityModule.log(TAG,self.users);
		if(self.users==null || !self.connection_available){
			UtilityModule.toast('Unable to fetch users! Please try again.')
		}else{
			UtilityModule.toast('User list updated.');
		}
		next();
	}

};

FirebaseModule.init();