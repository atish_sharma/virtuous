var TAG='NlpModule';

var NlpModule={

	sentiment:function(str){
		UtilityModule.log(TAG,'sentiment');
		var output=compendium.analyse(str);
		var analysis={
			'dirtiness':0,
			'politeness':0,
			'sentiment':0
		}
		for(var i=0;i<output.length;i++){
			var profile=output[i].profile;
			analysis.dirtiness+=profile.dirtiness;
			analysis.politeness+=profile.politeness;
			analysis.sentiment+=profile.sentiment;
		}
		return analysis;
	}

};