var TAG='FbModule';

var FbModule={

	permissions:'email,user_likes,user_posts',
	access_token:null,
	id:null,
	name:null,
	posts:[],
	likes:[],

	get_me:function(next){
		UtilityModule.log(TAG,'get_me');
		var self=this;
		FB.api('/me', {
	    	access_token: self.access_token
	    }, function(response) {
	    	UtilityModule.log(TAG,response);
	    	if(response){
	    		if(response.error){
	    			UtilityModule.log(TAG,response.error);
	    		}else{
			    	self.id=response.id;
			    	self.name=response.name;
	    		}
	    	}else{

	    	}
	    	next(response);
	    });
	},

	get_posts:function(next){
		UtilityModule.log(TAG,'get_posts');
		var self=this;
		FB.api('/me/posts', {
	    	access_token: self.access_token
	    }, function(response) {
	    	UtilityModule.log(TAG,response);
	    	if(response){
	    		if(response.error){
	    			UtilityModule.log(TAG,response.error);
	    		}else{
	    			self.posts=response.data;
	    		}
	    	}else{

	    	}
	    	next(response);
	    });
	},

	get_likes:function(next){
		UtilityModule.log(TAG,'get_likes');
		var self=this;
		FB.api('/me/likes', {
	    	access_token: self.access_token
	    }, function(response) {
	    	UtilityModule.log(TAG,response);
	    	if(response){
	    		if(response.error){
	    			UtilityModule.log(TAG,response.error);
	    		}else{
	    			self.likes=response.data;
	    		}
	    	}else{

	    	}
	    	next(response);
	    });
	}

};