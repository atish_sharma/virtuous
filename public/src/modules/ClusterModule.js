var TAG='ClusterModule';

var ClusterModule={

	tree:null,
	dendogram:null,
	depth:null,

	cluster:function(userList){
		UtilityModule.log(TAG,'cluster');
		var self=this;
		labels=[];
		vectors=[];
		likes=[];
		for(var i=0;i<userList.length;i++){
			likes.push(0);
			for(var j=0;j<userList.length;j++){
				if(i==j){
					continue;
				}
				var count=0;
				likesI=userList[i].likes;
				likesJ=userList[j].likes;
				for(var k=0;k<likesI.length;k++){
					//var found=false;
					for(var p=0;p<likesJ.length;p++){
						if(likesI[k]==likesJ[p]){
							//likes[i]+=1;
							//found=true;
							count+=1;
							break;
						}
					}
					/*
					if(!found){
						likes[i]-=1;
					}
					*/
				}
				if(likesJ.length>0){
					likes[i]+=count/likesJ.length;
				}
			}
			likes[i]/=userList.length;
		}
		for(var i=0;i<userList.length;i++){
			UtilityModule.log(TAG,userList[i].name)
			labels[i]='ID #'+i+' : '+userList[i].name;
			var analysis={
				'dirtiness':0,
				'politeness':0,
				'sentiment':0
			}	
			for(var j=0;j<userList[i].posts.length;j++){
				var postAnalysis=NlpModule.sentiment(userList[i].posts[j]);
				UtilityModule.log(TAG,'post analysis');
				UtilityModule.log(TAG,postAnalysis);
				analysis.dirtiness+=postAnalysis.dirtiness;
				analysis.politeness+=postAnalysis.politeness;
				analysis.sentiment+=postAnalysis.sentiment;
			}
			UtilityModule.log(TAG,'analysis');
			UtilityModule.log(TAG,analysis);
			vectors[i]=[
				analysis.sentiment,
				analysis.politeness,
				analysis.dirtiness,
				likes[i]
			];
		}
		UtilityModule.log(TAG,'likes')
		UtilityModule.log(TAG,likes);
		UtilityModule.log(TAG,'vectors');
		UtilityModule.log(TAG,vectors);
		var root = figue.agglomerate(labels,vectors,figue.EUCLIDIAN_DISTANCE,figue.AVERAGE_LINKAGE);
		UtilityModule.log(TAG,'root')
		UtilityModule.log(TAG,root);
		var dendogram=root.buildDendogram (5,true,true,true,true);
		UtilityModule.log(TAG,'dendogram');
		UtilityModule.log(TAG,dendogram);
		self.tree=root;
		self.dendogram=dendogram;
	},

	split:function(depth){
		UtilityModule.log(TAG,'split');
		var self=this;
		var clusters=[];
		self.splitTree(depth,self.tree,clusters);
		for(var i=0;i<clusters.length;i++){
			var node=clusters[i];
			var users=[];
			self.convertNodeToUsers(node,users);
			clusters[i]=users;
		}
		return clusters;
	},

	splitTree:function(depth,subtree,clusters){
		UtilityModule.log(TAG,'splitTree');
		var self=this;
		if(depth==0 || subtree.depth==0){
			clusters.push(subtree);
			return;
		}
		self.splitTree(depth-1,subtree.left,clusters);
		self.splitTree(depth-1,subtree.right,clusters);
	},

	convertNodeToUsers:function(node,users){
		UtilityModule.log(TAG,'convertNodeToUsers');
		var self=this;
		if(node.depth==0){
			var user={};
			var labels=node.label.split(' : ');
			user.name=labels[1];
			user.id=labels[0].split('#')[1];
			user.sentiment=node.centroid[0].toFixed(2);
			user.politeness=node.centroid[1].toFixed(2);
			user.dirtiness=node.centroid[2].toFixed(2);
			user.likes=node.centroid[3].toFixed(2);
			UtilityModule.log(TAG,user);
			user.posts=FirebaseModule.users[user.id].posts;
			users.push(user);
			return;
		}
		self.convertNodeToUsers(node.left,users);
		self.convertNodeToUsers(node.right,users);
	}


};