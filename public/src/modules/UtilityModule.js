var TAG='UtilityModule';
var LOG=true;

var UtilityModule={

	log:function(tag,data){
		if(LOG){
			console.log(tag,data);
		}
	},

	toast:function(message){
		UtilityModule.log(TAG,'toast');
		Materialize.toast(message,3000);
	}

};