app.config(function($routeProvider){
	$routeProvider
    .when('/', {
    	templateUrl:'static/views/login.html',
        controller:'LoginController'
    })
    .when('/login', {
    	templateUrl:'static/views/login.html',
        controller:'LoginController'
    })
    .when('/update', {
    	templateUrl:'static/views/update.html',
        controller:'UpdateController'
    })
    .when('/cluster', {
    	templateUrl:'static/views/cluster.html',
        controller:'ClusterController'
    })
    .when('/dendogram', {
        templateUrl:'static/views/dendogram.html',
        controller:'DendogramController'
    })
    .otherwise({
    	redirectTo:'/'
    });
});