app.directive('navigationFab',function(){
	return{
		restrict:'E',
		link:function(scope,element,attributes){

		},
		template:function(element,attributes){
			var t='';
			t+='<div class="fixed-action-btn" style="position:fixed;bottom:45px;right:24px;z-index:1000">';
			t+='<a class="btn-floating btn-large red">';
			t+='<i class="large material-icons">games</i>';
			t+='</a>';
			t+='<ul>';
			t+='<li><a class="btn-floating yellow darken-1"  href="#/update"><i class="material-icons">system_update_alt</i></a></li>';
			t+='<li><a class="btn-floating teal"  href="#/dendogram"><i class="material-icons">call_split</i></a></li>';
			t+='<li><a class="btn-floating green"  href="#/cluster"><i class="material-icons">view_module</i></a></li>';
			t+='<li><a class="btn-floating blue" href="#/login"><i class="material-icons">perm_identity</i></a></li>';
			t+='</ul>';
			t+='</div>';
			return t;
		}
	};
});