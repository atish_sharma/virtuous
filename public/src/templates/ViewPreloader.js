app.directive('viewPreloader',function(){
	return{
		restrict:'E',
		link:function(scope,element,attributes){

		},
		template:function(element,attributes){
			var t='';
			t+='<div class="progress" style="position:fixed;top:0px;left:0px;z-index:1000;width:100%;margin:0px;padding:0px">';
			t+='<div class="indeterminate">';
			t+='</div>';
			t+='</div>';
			return t;
		}
	};
});