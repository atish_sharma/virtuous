app.directive('preloader',function(){
	return{
		restrict:'E',
		link:function(scope,element,attributes){

		},
		template:function(element,attributes){
			var t='';
			t+='<div class="preloader-wrapper active">';
			t+='<div class="spinner-layer spinner-blue-only">';
			t+='<div class="circle-clipper left">';
			t+='<div class="circle"></div>';
			t+='</div><div class="gap-patch">';
			t+='<div class="circle"></div>';
			t+='</div><div class="circle-clipper right">';
			t+='<div class="circle"></div>';
			t+='</div></div></div>'
			return t;
		}
	};
});