var TAG='LoginController';

app.controller('LoginController',function($scope,$route,$routeParams,$location){

	$scope.showViewPreloader=false;
	$scope.showLoginPreloader=false;

	$scope.$on('$routeChangeStart', function(next,current) {
		UtilityModule.log(TAG,'$routeChangeStart');
       	$scope.showViewPreloader=true;
	});

	$scope.$on('$routeChangeSuccess', function(next,current) {
		UtilityModule.log(TAG,'$routeChangeSuccess');
       	$scope.showViewPreloader=false;
	});

	$scope.login=function(){
		UtilityModule.log(TAG,'login');
		$scope.showLoginPreloader=true;
		FirebaseModule.fb_login(function(error){
			$scope.showLoginPreloader=false;
			if(error){
				UtilityModule.toast('An error occurred! Please try again.');
			}else{
				UtilityModule.toast('Thank you for sharing your data with us. Please come back later to view your cluster.');
			}
			$scope.$apply();
		});
	};

});