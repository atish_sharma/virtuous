var TAG='DendogramController';

app.controller('DendogramController',function($scope,$route,$routeParams,$location){

	$scope.showViewPreloader=false;
	$scope.dendogram=null;

	$scope.$on('$routeChangeStart', function(next,current) {
		UtilityModule.log(TAG,'$routeChangeStart');
       	$scope.showViewPreloader=true;
	});

	$scope.$on('$routeChangeSuccess', function(next,current) {
		UtilityModule.log(TAG,'$routeChangeSuccess');
       	$scope.showViewPreloader=false;
       	$scope.dendogram=ClusterModule.dendogram;
	});

	$scope.noCluster=function(){
		UtilityModule.log(TAG,'noCluster');
		return ClusterModule.tree==null;
	};

});