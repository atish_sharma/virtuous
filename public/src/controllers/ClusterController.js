var TAG='ClusterController';

app.controller('ClusterController',function($scope,$route,$routeParams,$location){

	$scope.showViewPreloader=false;
	$scope.depth=ClusterModule.depth;
	$scope.clusters=[];

	$scope.$on('$routeChangeStart', function(next,current) {
		UtilityModule.log(TAG,'$routeChangeStart');
       	$scope.showViewPreloader=true;
	});

	$scope.$on('$routeChangeSuccess', function(next,current) {
		UtilityModule.log(TAG,'$routeChangeSuccess');
       	$scope.showViewPreloader=false;
       	try{
       		$scope.split();
       	}catch(err){
       		UtilityModule.log(TAG,err);
       	}
	});

	$scope.noCluster=function(){
		UtilityModule.log(TAG,'noCluster');
		return ClusterModule.tree==null;
	};

	$scope.split=function(){
		UtilityModule.log(TAG,'split');
		if($scope.depth==null || $scope.depth==''){
			return;
		}
		if(isNaN($scope.depth)){
			UtilityModule.toast('Please enter a number');
			return;
		}
		var depth=parseInt($scope.depth);
		if(depth<0){
			UtilityModule.toast('Please enter a number equal to or greater than 0');
			return;
		}
		ClusterModule.depth=depth;
		$scope.clusters=ClusterModule.split(ClusterModule.depth);
		UtilityModule.log(TAG,$scope.clusters);
	};

});