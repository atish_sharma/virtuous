var TAG='UpdateController';

app.controller('UpdateController',function($scope,$route,$routeParams,$location){

	$scope.showViewPreloader=false;
	$scope.showDataUpdatePreloader=false;

	$scope.$on('$routeChangeStart', function(next,current) {
		UtilityModule.log(TAG,'$routeChangeStart');
       	$scope.showViewPreloader=true;
	});

	$scope.$on('$routeChangeSuccess', function(next,current) {
		UtilityModule.log(TAG,'$routeChangeSuccess');
       	$scope.showViewPreloader=false;
	});

	$scope.download_users=function(){
		UtilityModule.log(TAG,'download_users');
		$scope.showDataUpdatePreloader=true;
		FirebaseModule.download_users(function(){
			if(FirebaseModule.users!=null){
				ClusterModule.cluster(FirebaseModule.users);
			}
			$scope.showDataUpdatePreloader=false;
		});
	};

});